// console.log('Hello World!');

	// Function 1
	function divisibleByTen(num) {
		console.log('The number you provided is ' + num);

		for (let ctr = num; ctr > 0; ctr--) {
			if (ctr <= 50) {
				console.log('The current value is at 50. Terminating the loop.');
				break;
			}
			else if (ctr % 10 === 0) {
				console.log('The number is divisible by 10. Skipping the number');
				continue;
			}
			else if (ctr % 5 === 0) {
				console.log(ctr);
			}
		}
	}

	let number = prompt('Enter a number:');
	divisibleByTen(number);
	// End of Function 1


	// Function 2
	function getVowels(str) {
		let finalString = '';

		for (let ctr = 0; ctr < str.length; ctr++) {
			if (str[ctr].toLowerCase() === 'a' || str[ctr].toLowerCase() === 'e' || str[ctr].toLowerCase() === 'i' || str[ctr].toLowerCase() === 'o' || str[ctr].toLowerCase() === 'u') {
				continue;
			} else {
				finalString += str[ctr];
			}
		}
		return finalString;
	}

	let result = getVowels('supercalifragilisticexpialidocious');
	console.log('supercalifragilisticexpialidocious');
	console.log(result);
	// End of Function 2